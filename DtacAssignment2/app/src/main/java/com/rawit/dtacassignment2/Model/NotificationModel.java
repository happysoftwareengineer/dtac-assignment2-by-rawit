package com.rawit.dtacassignment2.Model;

/**
 * Created by Rawit on 9/28/2017 AD.
 */

public class NotificationModel  {

    /**
     * Message : My message
     * Date : 28/09
     * Title : My title
     */

    private String Message;
    private String Date;
    private String Title;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }
}
