package com.rawit.dtacassignment2.SharePreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rawit.dtacassignment2.Common.Common;
import com.rawit.dtacassignment2.Model.NotificationModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rawit on 9/28/2017 AD.
 */

public class SharePreferenceManager {

    SharedPreferences mSharedPrefs;
    public SharePreferenceManager(Context context){

        mSharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
    }
    public void saveNotificationData(NotificationModel notificationModel)
    {
        Gson gson = new Gson();

        ArrayList<NotificationModel> listNoti = new ArrayList<NotificationModel>();

        String json = mSharedPrefs.getString(Common.LIST_NOTI_KEY,"");
        if(json.equals(""))
        {

            listNoti.add(notificationModel);

        }
        else
        {
            Type type = new TypeToken<List<NotificationModel>>(){}.getType();
            listNoti = gson.fromJson(json, type);

            listNoti.add(notificationModel);

        }

        String toJson = gson.toJson(listNoti);
        SharedPreferences.Editor prefsEditor = mSharedPrefs.edit();
        prefsEditor.putString(Common.LIST_NOTI_KEY, toJson);
        prefsEditor.commit();
    }
    public ArrayList<NotificationModel> getNotificationList()
    {
        String json = mSharedPrefs.getString(Common.LIST_NOTI_KEY,"");
        if(json.equals(""))
            return null;
        else{
            Gson gson = new Gson();

            Type type = new TypeToken<List<NotificationModel>>(){}.getType();
            ArrayList<NotificationModel> listNoti = gson.fromJson(json, type);
            return listNoti;
        }
    }
}
