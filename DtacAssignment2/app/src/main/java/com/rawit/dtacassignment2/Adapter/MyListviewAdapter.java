package com.rawit.dtacassignment2.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.rawit.dtacassignment2.Model.NotificationModel;
import com.rawit.dtacassignment2.R;

import java.util.ArrayList;

/**
 * Created by Rawit on 9/28/2017 AD.
 */

public class MyListviewAdapter extends BaseAdapter {

    ArrayList<NotificationModel> mListModel;

    public MyListviewAdapter(ArrayList<NotificationModel> listModel){
        mListModel = listModel;
    }

    @Override
    public int getCount() {
        return mListModel.size();
    }

    @Override
    public Object getItem(int i) {
        return mListModel.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View row_layout = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_layout,viewGroup,false);

        TextView title = (TextView)row_layout.findViewById(R.id.title);
        title.setText(mListModel.get(i).getTitle());

        TextView date = (TextView)row_layout.findViewById(R.id.date);
        date.setText(mListModel.get(i).getDate());

        TextView message = (TextView)row_layout.findViewById(R.id.message);
        message.setText(mListModel.get(i).getMessage());


        return row_layout;
    }
}
