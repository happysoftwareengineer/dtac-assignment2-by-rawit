package com.rawit.dtacassignment2.FCMService;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.rawit.dtacassignment2.Common.Common;
import com.rawit.dtacassignment2.Model.NotificationModel;
import com.rawit.dtacassignment2.SharePreference.SharePreferenceManager;

import static android.content.ContentValues.TAG;

/**
 * Created by Rawit on 9/28/2017 AD.
 */

public class AppMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);


        Log.d("DTAC", "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d("DTAC", "Message data payload: " + remoteMessage.getData());
            NotificationModel notificationModel = new NotificationModel();
            notificationModel.setTitle(remoteMessage.getData().get(Common.TITLE_KEY));
            notificationModel.setMessage(remoteMessage.getData().get(Common.MESSAGE_KEY));
            notificationModel.setDate(remoteMessage.getData().get(Common.DATE_KEY));
            saveNotification(notificationModel);

            boardcastMessage(notificationModel);


        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d("DTAC", "Message Notification Body: " + remoteMessage.getNotification().getBody());
            Log.d("DTAC", "Message Notification Title: " + remoteMessage.getNotification().getTitle());
        }


    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }
    private void saveNotification(NotificationModel notificationModel)
    {
        SharePreferenceManager sharePreferenceManager = new SharePreferenceManager(getApplicationContext());
        sharePreferenceManager.saveNotificationData(notificationModel);
    }

    private void boardcastMessage(NotificationModel notificationModel) {
        Intent intent = new Intent(Common.GOT_NOTI_KEY);
        intent.putExtra(Common.TITLE_KEY,notificationModel.getTitle());
        intent.putExtra(Common.MESSAGE_KEY,notificationModel.getMessage());

        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getApplicationContext());
        manager.sendBroadcast(intent);
    }
}
