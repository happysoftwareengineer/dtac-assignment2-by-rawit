package com.rawit.dtacassignment2.Views;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.rawit.dtacassignment2.Adapter.MyListviewAdapter;
import com.rawit.dtacassignment2.Common.Common;
import com.rawit.dtacassignment2.Model.NotificationModel;
import com.rawit.dtacassignment2.Presenter.MainPresenter;
import com.rawit.dtacassignment2.Presenter.MainPresenterInteractorImp;
import com.rawit.dtacassignment2.R;
import com.rawit.dtacassignment2.SharePreference.SharePreferenceManager;
import com.rawit.dtacassignment2.Views.NotificationListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements ListView.OnItemClickListener , NotificationListView {

    ListView mListView;
    MyListviewAdapter mAdapter;
    TextView mTextViewNodata;
    ArrayList<NotificationModel> mNotificationModelList;

    MainPresenterInteractorImp mInteractor;
    private MainPresenter mPresenter;

    private BroadcastReceiver mNotiReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mInteractor = new MainPresenterInteractorImp();
        mPresenter = new MainPresenter(mInteractor,this);
        mPresenter.bind(this);

        bindingView();
        checkExtra();
        checkFCMToken();


        mPresenter.refreshUI();


        mNotiReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(Common.GOT_NOTI_KEY))
                {
                    mPresenter.showDialog(intent);
                }
            }
        };

    }
    @Override
    protected void onStart() {
        super.onStart();

        IntentFilter intentFilter = new IntentFilter(Common.GOT_NOTI_KEY);
        LocalBroadcastManager.getInstance(this).registerReceiver((mNotiReceiver),intentFilter);
    }
    private void checkExtra()
    {
        if (getIntent().getExtras() != null) {
            mPresenter.saveNotification(getIntent(),this);
        }
    }

    private void checkFCMToken()
    {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("DTAC","FCM Token : "+ refreshedToken);
    }

    private  void bindingView()
    {
        mTextViewNodata = (TextView)findViewById(R.id.noData);

        mListView = (ListView)findViewById(R.id.listView);
        mListView.setOnItemClickListener(this);

    }
//    private void showDialog(Intent intent)
//    {
//        AlertDialog.Builder builder;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
//        } else {
//            builder = new AlertDialog.Builder(this);
//        }
//        builder.setTitle(intent.getStringExtra(Common.TITLE_KEY))
//                .setMessage(intent.getStringExtra(Common.MESSAGE_KEY))
//                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        // continue with delete
//                        mPresenter.refreshUI();
//                    }
//                })
//                .setIcon(android.R.drawable.ic_dialog_alert)
//                .show();
//    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

            mPresenter.manageExpandRow(view,position);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNotiReceiver);
        mPresenter.unbind();
    }

    @Override
    public void updateUI(ArrayList<NotificationModel> listNotificationModels) {

        if(listNotificationModels !=null) {
            mTextViewNodata.setVisibility(View.INVISIBLE);
            mAdapter = new MyListviewAdapter(listNotificationModels);
            mListView.setAdapter(mAdapter);
        }
        else
            mTextViewNodata.setVisibility(View.VISIBLE);

    }

    @Override
    public void manageExpandRow(View view, int position) {

        ViewGroup.LayoutParams lp = view.getLayoutParams();
        if(lp.height != ViewGroup.LayoutParams.WRAP_CONTENT)
        {
            lp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        }
        else
        {
            lp.height = (int) getResources().getDimension(R.dimen.row_height);
        }
        view.setLayoutParams(lp);
        view.requestLayout();
    }

    @Override
    public void showDialog(Intent intent) {
                AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle(intent.getStringExtra(Common.TITLE_KEY))
                .setMessage(intent.getStringExtra(Common.MESSAGE_KEY))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        mPresenter.refreshUI();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert);

        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
}
