package com.rawit.dtacassignment2.Common;

/**
 * Created by Rawit on 9/28/2017 AD.
 */

 public class Common {

    public static String TITLE_KEY = "Title";
    public static String MESSAGE_KEY = "Message";
    public static String DATE_KEY = "Date";

    public static String LIST_NOTI_KEY = "notificationlist";

    public static String GOT_NOTI_KEY = "GOTNOTI";
}
