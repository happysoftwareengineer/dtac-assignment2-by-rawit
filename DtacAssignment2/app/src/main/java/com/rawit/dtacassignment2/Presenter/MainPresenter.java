package com.rawit.dtacassignment2.Presenter;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.rawit.dtacassignment2.Model.NotificationModel;
import com.rawit.dtacassignment2.Views.NotificationListView;

import java.util.ArrayList;

/**
 * Created by Rawit on 9/28/2017 AD.
 */

public class MainPresenter {

    MainPresenterInteractor mInteractor;
    NotificationListView mView;
    Context mContext;

    public MainPresenter(MainPresenterInteractor interactor , Context context) {
        mInteractor = interactor;
        mContext = context;
    }
    public void bind(NotificationListView view) {
        this.mView = view;
    }

    public void unbind() {
        mView = null;
    }
    public void refreshUI()
    {
        ArrayList<NotificationModel> listNotification = mInteractor.getListNotification(mContext);
        mView.updateUI(listNotification);
    }
    public void saveNotification(Intent intent,Context context)
    {
        mInteractor.saveNotification(intent,context);
    }
    public void manageExpandRow(View view,int position)
    {
        mView.manageExpandRow(view,position);
    }
    public void showDialog(Intent intent)
    {
        mView.showDialog(intent);
    }
}
