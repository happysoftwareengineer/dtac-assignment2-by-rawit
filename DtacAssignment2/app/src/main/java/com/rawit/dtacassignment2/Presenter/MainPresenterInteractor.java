package com.rawit.dtacassignment2.Presenter;

import android.content.Context;
import android.content.Intent;

import com.rawit.dtacassignment2.Model.NotificationModel;

import java.util.ArrayList;

/**
 * Created by Rawit on 9/28/2017 AD.
 */

public interface MainPresenterInteractor {
    ArrayList<NotificationModel> getListNotification(Context context);

    void saveNotification(Intent intent,Context context);
}
