package com.rawit.dtacassignment2.Views;

import android.content.Intent;
import android.view.View;

import com.rawit.dtacassignment2.Model.NotificationModel;

import java.util.ArrayList;

/**
 * Created by Rawit on 9/28/2017 AD.
 */

public interface NotificationListView {

    void updateUI(ArrayList<NotificationModel> listNotificationModels);

    void manageExpandRow(View view,int position);

    void showDialog(Intent intent);
}
