package com.rawit.dtacassignment2.Presenter;

import android.content.Context;
import android.content.Intent;

import com.rawit.dtacassignment2.Common.Common;
import com.rawit.dtacassignment2.Model.NotificationModel;
import com.rawit.dtacassignment2.SharePreference.SharePreferenceManager;

import java.util.ArrayList;

/**
 * Created by Rawit on 9/28/2017 AD.
 */

public class MainPresenterInteractorImp implements MainPresenterInteractor {

    @Override
    public ArrayList<NotificationModel> getListNotification(Context context) {
        SharePreferenceManager sharePreferenceManager = new SharePreferenceManager(context);

        return sharePreferenceManager.getNotificationList();
    }

    @Override
    public void saveNotification(Intent intent,Context context) {

        NotificationModel notificationModel = new NotificationModel();
        notificationModel.setTitle(intent.getExtras().getString(Common.TITLE_KEY));
        notificationModel.setMessage(intent.getExtras().getString(Common.MESSAGE_KEY));
        notificationModel.setDate(intent.getExtras().getString(Common.DATE_KEY));

        SharePreferenceManager sharePreferenceManager = new SharePreferenceManager(context);
        sharePreferenceManager.saveNotificationData(notificationModel);

    }
}
