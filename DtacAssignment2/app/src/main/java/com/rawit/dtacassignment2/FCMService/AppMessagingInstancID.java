package com.rawit.dtacassignment2.FCMService;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import static android.content.ContentValues.TAG;

/**
 * Created by Rawit on 9/28/2017 AD.
 */

public class AppMessagingInstancID extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("DTAC", "Refreshed token: " + refreshedToken);
    }
}
